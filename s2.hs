module TypedLambda1 where

data Term = Literal String Type
          | Abs String Type Term
          | App Term Term
          | Tru
          | Fls
          | Err

data Type = TBool
          | TInt
          | TComplex Type Type
          | TError
          deriving(Eq)

getType :: Term -> Type
getType Tru  = TBool
getType Fls = TBool
getType Err = TError
getType (Literal _ t) = t
getType (Abs _ type1 term) = TComplex (type1) (getType term)
getType (App (Abs arg type1 absTerm) term) | type1 == (getType term) = getType (passArgs absTerm arg term)
getType (App (Abs _ _ _) _) | otherwise = TError
getType (App _ _) = TError


passArgs :: Term -> String -> Term -> Term
passArgs (Literal var _) arg1 term | var == arg1 = term
passArgs (Literal var body) _ _ | otherwise = (Literal var body)
passArgs (Abs arg type1 body) arg1 term = Abs arg type1 (passArgs body arg1 term)
passArgs (App t1 t2) arg term = App (passArgs t1 arg term) (passArgs t2 arg term)
passArgs t a l = Err

toString :: Type -> String
toString TError = "TError"
toString TBool = "TBool"
toString (TComplex t1 t2) = "(" ++ (toString t1) ++ " -> " ++ (toString t2) ++ ")"
toString TInt = "TInt"


-- Example of work
-- main = print $ toString $ getType $
--      App (Abs "x" (TComplex TInt TBool) (Literal "y" TInt)) (Literal "z" (TComplex TInt TBool))