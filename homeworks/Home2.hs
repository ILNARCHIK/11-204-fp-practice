deleteInt :: Int -> [Int] -> [Int]

deleteInt _ [] = []

deleteInt arg (head:tail) | head == arg = deleteInt arg tail
                          | otherwise = head:(deleteInt arg tail)

findIndicesInt :: Int -> [Int] -> [Int]

findIndicesInt _ [] = []

findIndicesInt arg (head:tail) = findIndices' arg (head:tail) 0

    where findIndices' _ [] _ = []
	
          findIndices' arg (head:tail) ind | arg == head = ind : (findIndices' arg tail (ind+1))
                                           | otherwise = findIndices' arg tail (ind+1)


tribo :: Int -> Int

tribo 0 = 1
tribo 1 = 1
tribo 2 = 1
tribo 3 = 3

tribo n = tribo (n-3) + tribo (n-2) + tribo (n-1)
